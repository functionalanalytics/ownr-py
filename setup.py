import setuptools
with open("Readme.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
     name='ownr',  
     version='0.1',
     scripts=['bin/ownr'] ,
     author="Gergely Mark",
     author_email="gergely.mark@functionalanalytics.nl",
     description="ownR platform SDK",
     long_description=long_description,
     long_description_content_type="text/markdown",
     use_2to3=True,     
     packages=setuptools.find_packages(),
     install_requires=['docker', 'requests', 'ruamel.yaml', 'yaspin', 'pygit2', 'influxdb', 'kubernetes', 'python-dxf'],
     include_package_data = True
 )