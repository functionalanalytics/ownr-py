# ownr python SDK
Python interface including a CLI interface for managing an ownR installation
## Installation
pip install git+https://bitbucket.org/functionalanalytics/ownr-py.git
## Usage
ownr config -l <ownr_license>
ownr platform start
ownr platform stop
## License
MIT
