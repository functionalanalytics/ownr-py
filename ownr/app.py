import argparse
import logging
import os
import shutil

from .config import Config
from .keycloak import Keycloak
from .platform import Platform
from .utils import StringYAML
from .stacks.apps import start_app, stop_app, restart_app, rescale_app
from .git import Git
from ruamel.yaml import YAML

logger = logging.getLogger(__name__)


class App():
  def __init__(self, args, default_config = {}):
    if isinstance(args, argparse.Namespace):
      args = vars(args)

    self.config = Config.from_yaml(args.get("config_dir"))

    self.name = args.get("name") or default_config.get("name" , "")
    self.language = args.get("language") or default_config.get("language", "")
    self.description = args.get("description") or default_config.get("description", "")
    self.language_version = args.get("language_version") or default_config.get("language_version", "")
    self.app_type = args.get("app_type") or default_config.get("app_type", "")
    self.replicas = args.get("replicas") or default_config.get("replicas", 1)
    self.target = args.get("target") or default_config.get("target", "")
    self.api_key = args.get("api_key") or default_config.get("api_key", "")
    self.deploy_version = args.get("deploy_version") or default_config.get("deploy_version", "")
    self.env = args.get("env") or default_config.get("env", [])
    self.mount = args.get("mount") or default_config.get("mount", [])


  @property
  def stack_name(self):
    return f"{self.config.stack_name}_app_{self.name}"


  def to_dict(self):
    return {"version": 1, 
            "name": self.name,
            "description": self.description,
            "language": self.language,
            "language_version": self.language_version,
            "app_type": self.app_type,
            "api_key": self.api_key,
            "replicas": self.replicas,
            "target": self.target,
            "deploy_version": self.deploy_version,
            "env": self.env,
            "mount": self.mount
    }


  def to_yaml(self):
    return StringYAML().dump(self.to_dict())  


  @staticmethod
  def from_yaml(yaml_file, config_dir):
    try:
      config_fp = open(yaml_file, "r")
      data = YAML().load(config_fp)    
      data["config_dir"] = config_dir
      config_fp.close()
    except Exception as e:
      raise e

    return App(data)


  @staticmethod
  def create(args):
    app = App(args)
    App.save_app(app)    
    Git.init_app(app)


  @staticmethod
  def get_parser(management_commands):
    parser = management_commands.add_parser("app", help="manage an application running on the ownR platform")
    parser._positionals.title = argparse.SUPPRESS
    commands = parser.add_subparsers(help="", metavar="Command", dest="subcommand")
    
    create_command = commands.add_parser("create", help="create an application")
    create_command.add_argument("--name", required=True, help="name of the application")
    create_command.add_argument("--language", required=True, choices=["R", "python"], metavar="language", help="programming language used by the app")
    create_command.add_argument("--language-version", required=True, help="version of the language used by the app")
    create_command.add_argument("--app-type", required=True, metavar="app-type", choices=["API", "web"], help="type of the application")
    create_command.add_argument("--api-key", help="API key for API apps")
    create_command.add_argument("--replicas", required=True, help="numberof replicas")
    create_command.add_argument("--target", help="nodes usable by the app")    

    start_command = commands.add_parser("start", help="start an application")
    start_command.add_argument("--name", required=True, help="name of the application")

    stop_command = commands.add_parser("stop", help="stop an application")
    stop_command.add_argument("--name", required=True, help="name of the application")

    rescale_command = commands.add_parser("rescale", help="rescale an application")
    rescale_command.add_argument("--name", required=True, help="name of the application")
    rescale_command.add_argument("--replicas", required=True, help="number of replicas")

    deploy_command = commands.add_parser("deploy", help="deploy an application")
    deploy_command.add_argument("--name", required=True, help="name of the application")
    deploy_command.add_argument("--version", help="specific version")

    commands.add_parser("list", help="list all application")    


  @staticmethod
  def start(args):
    config = Config.from_yaml(args.config_dir)
    app = App.from_yaml(os.path.join(config.apps_dir, f"{args.name}.yaml"), config.config_dir)
    start_app(app)
      

  @staticmethod
  def stop(args):
    config = Config.from_yaml(args.config_dir)
    app = App.from_yaml(os.path.join(config.apps_dir, f"{args.name}.yaml"), config.config_dir)
    stop_app(app)


  @staticmethod
  def deploy(args):
    if isinstance(args, argparse.Namespace):
      args = vars(args)
    config = Config.from_yaml(args.get("config_dir"))
    app = App.from_yaml(os.path.join(config.apps_dir, f"{args.get('name')}.yaml"), config.config_dir)
    stop_app(app)

    if args.get("version", False):
      app.deploy_version = args.get("version")
      App.save_app(app)

    start_app(app)


  @staticmethod
  def get_app(config, name):
    config = os.path.join(config.apps_dir, f"{name}.yaml")
    if not os.path.isfile(config):
      logger.error(f"ownR app {config} does not exists! See 'ownr app create -h'") 
      exit(1)

    try:
      config_fp = open(config, "r")
      data = YAML().load(config_fp)    
      data["config_dir"] = config.config_dir
      config_fp.close()
    except Exception as e:
      raise e

    return App(data)


  @staticmethod
  def save_app(app):
    try:
      config_file = os.path.join(app.config.apps_dir, f"{app.name}.yaml")
      config_fp = open(config_file, "w")
      YAML().dump(app.to_dict(), config_fp)
      config_fp.close()
      Keycloak(app.config.keycloak_user, 
               app.config.keycloak_password, 
               app.config.hostname).create_role(f"ownr_role_{app.name}", 
                                            f"Authorization role for app {app.name}", 
                                            "ownr")
    except Exception as e:
      raise e
    

  @staticmethod
  def remove_app(config, app):
    try:
      app_yaml = os.path.join(config.apps_dir, f"{app.name}.yaml")
      app_dir = os.path.join(config.apps_dir, f"{app.name}.git")
      if os.path.isdir(app_dir):
        shutil.rmtree(app_dir)
      if os.path.isfile(app_yaml):
        os.unlink(app_yaml)      
      Keycloak(config.keycloak_user, 
               config.keycloak_password, 
               config.hostname).remove_role(f"ownr_role_{app.name}", 
                                             "ownr")         
      stop_app(app)                                                                                    
    except Exception as e:
      raise e


  @staticmethod
  def list_apps(config):
    apps = []
    for f in os.listdir(config.apps_dir):
      if os.path.isfile(os.path.join(config.apps_dir, f)):
        try:
          apps.append(App.from_yaml(os.path.join(config.apps_dir, f), config.config_dir).to_dict())
        except Exception as e:
          logger.warning(f"Failed to load app definition: {e}")
    return apps 


  @staticmethod
  def rescale(args):
    config = Config.from_yaml(args.config_dir)
    app = App.get_app(config, args.name)    
    app.replicas = int(args.replicas)
    App.save_app(app)
    rescale_app(app)


  @staticmethod
  def list(args, format=True):
    config = Config.from_yaml(args.config_dir)
    if not format:
      return App.list_apps(config)

    for name in App.list_apps(config):
      print(name)