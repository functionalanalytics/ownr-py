import requests
import json
import base64
import logging
from http.cookies import SimpleCookie
logger = logging.getLogger(__name__)


class Keycloak():

  def __init__(self, username, password, base_url):
    self.username = username
    self.password = password
    if base_url[-1] != "/":
      base_url = base_url + "/"
    self.base_url = base_url
    self.url = f"{base_url}auth"    


  @property
  def token(self):
    data = {"username": self.username,
          "password": self.password,
          "grant_type": "password",
          "client_id": "admin-cli"}

    response = requests.post(f"{self.url}/realms/master/protocol/openid-connect/token",
                           data=data,
                           verify=False,
                           headers={"Accept": "application/json"}).json()

    if not isinstance(response, dict):
      raise PermissionError("Failed to get access token")    
    if not response.get("access_token", None):
      raise PermissionError("Failed to get access token")

    return response.get("access_token")


  def __del__(self):
    self.logout()

  def logout(self):
    pass


  def create_realm(self, realm, display_name):
    display_name = display_name.replace('"', '')
    data = {"realm": realm,
            "displayName": display_name,
            "enabled": True}

    response = requests.post(f"{self.url}/admin/realms",
                            data=json.dumps(data),
                            verify=False,
                            headers={"Accept": "application/json",
                                    "Authorization": f"Bearer {self.token}",
                                    "Content-type": "application/json"})
    return response

  def update_realm(self, realm, display_name="ownR platform"):    
    display_name = display_name.replace('"', '')
    data = {"realm": realm,
            "displayName": display_name,
            "enabled": True}

    response = requests.put(f"{self.url}/admin/realms",
                            data=json.dumps(data),
                            verify=False,
                            headers={"Accept": "application/json",
                                    "Authorization": f"Bearer {self.token}",
                                    "Content-type": "application/json"})

    return response


  def create_role(self, role_name, description, realm):
    data = {"name": role_name,
            "description": description}

    response = requests.post(f"{self.url}/admin/realms/{realm}/roles/",
                             data=json.dumps(data),
                             verify=False,
                             headers={"Accept": "application/json",
                                      "Authorization": f"Bearer {self.token}",
                                      "Content-type": "application/json"})

    return response


  def remove_role(self, role_name, realm):
    response = requests.delete(f"{self.url}/admin/realms/{realm}/roles/{role_name}",
                               verify=False,
                               headers={"Accept": "application/json",
                                        "Authorization": f"Bearer {self.token}",
                                        "Content-type": "application/json"})
    return response

  
  def create_platform_client(self, hostname, realm):
    data = {"clientId": "platform",
            "redirectUris": [f"{hostname}/*"],
            "publicClient": True,
            "directAccessGrantsEnabled": True}

    response = requests.post(f"{self.url}/admin/realms/{realm}/clients/",
                             data=json.dumps(data),
                             verify=False,
                             headers={"Accept": "application/json",
                                      "Authorization": f"Bearer {self.token}",
                                      "Content-type": "application/json"})
                  
    return response


  def create_api_client(self):
    data = {"clientId": "api",
            "redirectUris": ["/*"],
            "publicClient": False,
            "serviceAccountsEnabled": True}

    response = requests.post(f"{self.url}/admin/realms/master/clients/",
                             data=json.dumps(data),
                             verify=False,
                             headers={"Accept": "application/json",
                                      "Authorization": f"Bearer {self.token}",
                                      "Content-type": "application/json"})
                  
    return response

  
  def get_client_id(self, client_id, realm):
    response = requests.get(f"{self.url}/admin/realms/{realm}/clients",
                             verify=False,
                             headers={"Accept": "application/json",
                                      "Authorization": f"Bearer {self.token}",
                                      "Content-type": "application/json"}).json()
                  
    clients = response.json()

    for client in clients:
      if client.get("clientId") == client_id:
        return client.get("id")

    return False


  def get_service_account_id(self, client_id, realm):
    response = requests.get(f"{self.url}/admin/realms/{realm}/clients/{self.get_client_id(client_id, realm)}/service-account-user",
                             verify=False,
                             headers={"Accept": "application/json",
                                      "Authorization": f"Bearer {self.token}",
                                      "Content-type": "application/json"}).json()
    
    if not response.get("id", None):
        return False
    
    return response.get("id")

  
  def get_service_account_secret(self, client_id, realm):
    response = requests.get(f"{self.url}/admin/realms/{realm}/clients/{self.get_client_id(client_id, realm)}/client-secret",
                          verify=False,
                          headers={"Accept": "application/json",
                                  "Authorization": f"Bearer {self.token}",
                                  "Content-type": "application/json"}).json()
    
    if not response.get("value", None):
      return False

    return response.get("value")

  
  def create_user(self, username, realm, enabled = True, password = None):
    data = {"username": username,
            "enabled": enabled}

    if not password is None:
      data["credentials"] = [{"type": "password", "value": password}]

    response = requests.post(f"{self.url}/admin/realms/{realm}/users",
                          data=json.dumps(data),
                          verify=False,
                          headers={"Accept": "application/json",
                                  "Authorization": f"Bearer {self.token}",
                                  "Content-type": "application/json"})

    return response


  def get_all_users(self, realm):
    response = requests.get(f"{self.url}/admin/realms/{realm}/users?briefRepresentation=false",
                          verify=False,
                          headers={"Accept": "application/json",
                                  "Authorization": f"Bearer {self.token}",
                                  "Content-type": "application/json"}).json()
    return response                                  


  def get_user_id(self, username, realm):
    users = self.get_all_users(realm)
    
    for user in users:
      if user.get("username", "") == username:
        return user.get("id")
    
    return False


  def get_user_info(self, userid, realm):
    response = requests.get(f"{self.url}/admin/realms/{realm}/users/{userid}",
                          verify=False,
                          headers={"Accept": "application/json",
                                  "Authorization": f"Bearer {self.token}",
                                  "Content-type": "application/json"}).json()
    return response


  def set_user_platform_token(self, username, realm, api_key):
    userid = self.get_user_id(username, realm)
    user_info = self.get_user_info(userid, realm)
    
    if not user_info.get("attributes", False):
      user_info["attributes"] = {}

    user_info["attributes"]["ownr-platform-key"] = api_key

    response = requests.put(f"{self.url}/admin/realms/{realm}/users/{userid}",
                          verify=False,
                          data=json.dumps(user_info),
                          headers={"Accept": "application/json",
                                  "Authorization": f"Bearer {self.token}",
                                  "Content-type": "application/json"})

    return response  


  def get_role_info(self, role_name, realm):
    response = requests.get(f"{self.url}/admin/realms/{realm}/roles/{role_name}",
                            verify=False,
                            headers={"Accept": "application/json",
                                     "Authorization": f"Bearer {self.token}"}).json()
    return response


  def add_role_to_user(self, role_name, userid, realm):
    data = [self.get_role_info(role_name, realm)]

    response = requests.post(f"{self.url}/admin/realms/{realm}/users/{userid}/role-mappings/realm",
                          data=json.dumps(data),
                          verify=False,
                          headers={"Accept": "application/json",
                                  "Authorization": f"Bearer {self.token}",
                                  "Content-type": "application/json"})
    return response  


  def remove_role_from_user(self, rolename, userid, realm):
    data = [self.get_role_info(rolename, realm)]
    response = requests.delete(f"{self.url}/admin/realms/{realm}/users/{userid}/role-mappings/realm",
                          data=json.dumps(data),
                          verify=False,
                          headers={"Accept": "application/json",
                                  "Authorization": f"Bearer {self.token}",
                                  "Content-type": "application/json"})
    return response

  
  def is_token_valid(self, token):
    try:
      response = requests.get(f"{self.url}/realms/ownr/account/",
                              verify=False,
                              headers={"Accept": "application/json",
                                       "Authorization": f"Bearer {token}"})
      response = response.json()
    except Exception as e:
      logger.error(e)
      return False

    return "username" in response.keys()  


  @staticmethod
  def get_username_from_token(token):
    info = token.split(".")[1]
    return json.loads(base64.b64decode(f"{info}==")).get("preferred_username")


  @staticmethod
  def get_userid_from_token(token):
    info = token.split(".")[1]
    return json.loads(base64.b64decode(f"{info}==")).get("sub")


  @staticmethod
  def get_roles_from_token(token):
    info = token.split(".")[1]
    return json.loads(base64.b64decode(f"{info}==")).get("realm_access", {}).get("roles", [])
  

  @staticmethod
  def is_admin(token):
    roles = Keycloak.get_roles_from_token(token)
    try:
      roles.index("ownr-admin")
      return True
    except ValueError:
      return False


  @staticmethod
  def is_dev(token):
    if Keycloak.is_admin(token):
      return True

    info = token.split(".")[1]    
    roles = json.loads(base64.b64decode(f"{info}==")).get("realm_access", {}).get("roles", [])
    try:      
      roles.index("ownr-dev")
      return True
    except ValueError:
      return False


  @staticmethod
  def get_token_from_cookie(cookie):
    parsed_cookie = SimpleCookie()
    parsed_cookie.load(cookie)
    cookies = {}
    for cookie in parsed_cookie.values():
      cookies[cookie.key] = cookie.coded_value 

    return cookies.get("ownr", False)


  def get_internal_userid(self):
    return self.get_user_id("ownr-internal", "ownr")

  
  def get_role_users(self, role_name, realm):
    response = requests.get(f"{self.url}/admin/realms/{realm}/roles/{role_name}/users",
                            verify=False,
                            headers={"Accept": "application/json",
                                    "Authorization": f"Bearer {self.token}",
                                    "Content-type": "application/json"}).json()

    return response                     


  def impersonate(self, username, realm):
    userid = self.get_user_id(username, realm)
    response = requests.post(f"{self.url}/admin/realms/{realm}/users/{userid}/impersonation",
                            verify=False,
                            headers={"Accept": "application/json",
                                    "Authorization": f"Bearer {self.token}",
                                    "Content-type": "application/json"})
    return response


  def user_roles(self, userid):
    return requests.get(f"{self.url}/admin/realms/ownr/users/{userid}/role-mappings/realm",
                        verify=False,
                        headers={"Accept": "application/json",
                                 "Authorization": f"Bearer {self.token}",
                                 "Content-type": "application/json"}).json()


  def user_has_role(self, userid, role):
    response = self.user_roles(userid)

    for user_role in response:
      if user_role.get("name") == role:
        return True
    
    return False

  def validate_user(self, user, pwd):
    data = {"client_id": "platform",
            "grant_type": "password",
            "username": user, 
            "password": pwd
            }

    try:
      result = requests.post(f"{self.url}/realms/ownr/protocol/openid-connect/token", verify=False,
                            data = data).json().get("access_token")       
      return True if result else False
      
    except Exception:
      return False


  def get_admin_client_id(self):
    clients = requests.get(f"{self.url}/admin/realms/master/clients",
                        verify=False,
                        headers={"Accept": "application/json", "Authorization": f"Bearer {self.token}"}).json()
    for client in clients:
      if client.get("name", "") == "${client_security-admin-console}":
        return client.get("id", None)
    
    return None


  def setup_admin_client(self, hostname):
    client_id = self.get_admin_client_id()
    client_data = requests.get(f"{self.url}/admin/realms/master/clients/{client_id}",
                               verify=False,
                               headers={"Accept": "application/json", 
                                        "Authorization": f"Bearer {self.token}"}).json()

    client_data["rootUrl"] = hostname

    return requests.put(f"{self.url}/admin/realms/master/clients/{client_id}", data=json.dumps(client_data),
                        verify=False,
                        headers={"Content-type": "application/json",
                                 "Authorization": f"Bearer {self.token}"})


    