""" General app settings. """
app_name = 'fokkerexample'
# app_base_url = f'/deployed/web/{app_name}/'  # Must end with /
app_base_url = '/'  # Must end with /

# Other settings can be defined here
