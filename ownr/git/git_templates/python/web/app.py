""" App and server are defined here. We define these in a separate file so that we can separate
callbacks into different files and import them in the main .py file. """
import dash
import dash_bootstrap_components as dbc
from flask import Flask

from settings import app_base_url

# Define flask app manually, this way we can set routes for file downloads
server = Flask(__name__)
app = dash.Dash(__name__,
                server=server,
                meta_tags=[{"name": "viewport", "content": "width=device-width"}],
                url_base_pathname=app_base_url,
                external_stylesheets=[dbc.themes.BOOTSTRAP]
)

# Flask routes (e.g. file downloads) must be defined here

# Set to false if layout elements are not in app on first load, e.g. layout components
# loaded by a callback
app.config.suppress_callback_exceptions = True

server = app.server
demo_mode = True
