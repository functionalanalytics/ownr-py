from app import app
from dash.dependencies import Input, Output, State


@app.callback(
    Output('number-input', 'value'),
    [Input('add-one-button', 'n_clicks')],
    [State('number-input', 'value')]
)
def add_one(n_clicks, number_input_value):
    if n_clicks is None:
        return 0
    else:
        return number_input_value + 1