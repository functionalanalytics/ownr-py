# We need everything from app.py, otherwise app wont run in docker container
from app import *

# Import callbacks defined in files
import callbacks.main_page_callbacks

# Other imports
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

# Main layout of the app
app.layout = dbc.Container([
    dbc.Col([
        dbc.Row([
            html.H1("Hello, World")
        ]),
        dbc.Row([
            dcc.Input(
                id="number-input",
                type="number",
                placeholder="Enter number",
                value=0
            ),
            html.Button(
                children="Add one",
                id="add-one-button",
            )
        ])
    ])
])


# Running the server
if __name__ == "__main__":
    app.run_server(debug=True)
