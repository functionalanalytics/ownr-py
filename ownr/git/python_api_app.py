def get_app_py(app):
  return f"""import logging
logger = logging.getLogger(__name__)

def echo(n):
  logger.info(n)
  return n

"""