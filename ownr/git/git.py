import pygit2
import tempfile
import os
import stat
import shutil
from distutils.dir_util import copy_tree
from datetime import datetime, timezone, timedelta
from . import python_web_app, python_api_app, r_api_app, r_web_app


class Git():

  @staticmethod
  def get_template_dir(app):
    base_dir = os.path.join(os.path.dirname(__file__), "git_templates")
    return os.path.join(base_dir, app.language, app.app_type.lower())


  @staticmethod
  def setup_template(app, dir):
    if app.language == "python":
      if app.app_type == "Web":
        copy_tree(Git.get_template_dir(app), dir)
        app_py = open(os.path.join(dir, f"{app.name}.py"), "w")
        app_py.write(python_web_app.get_app_py(app))
        app_py.close()
      if app.app_type == "API":
        app_py = open(os.path.join(dir, f"{app.name}.py"), "w")
        app_py.write(python_api_app.get_app_py(app))
        app_py.close()
      if app.app_type == "NotebookHTML":
        copy_tree(Git.get_template_dir(app), dir)
        os.rename(os.path.join(dir, "app.ipynb"), os.path.join(dir, f"{app.name}.ipynb"))
      if app.app_type == "Notebook":
        copy_tree(Git.get_template_dir(app), dir)
        os.rename(os.path.join(dir, "app.ipynb"), os.path.join(dir, f"{app.name}.ipynb"))
    if app.language == "R":
      if app.app_type == "Web":
        copy_tree(Git.get_template_dir(app), dir)
        ui_r = open(os.path.join(dir, "ui.R"), "w")
        ui_r.write(r_web_app.get_ui_r(app))
        ui_r.close()
      if app.app_type == "API":
        copy_tree(Git.get_template_dir(app), dir)
        desc = open(os.path.join(dir, "DESCRIPTION"), "w")
        desc.write(r_api_app.get_description(app))
        desc.close()


  @staticmethod
  def get_repository(app):
    git_path = os.path.join(app.config.apps_dir, f"{app.name}.git")
    return pygit2.Repository(git_path)

  @staticmethod
  def get_commit_hook(app):
    return f"""#!/bin/bash
while read oldrev newrev ref
do
    if [[ $ref =~ .*/master$ ]];
    then
        source /opt/ownr/admin/venv/bin/activate
        ownr app deploy --name {app.name} --version $newrev
    fi
done
    """

  @staticmethod
  def init_app(app):
    git_path = os.path.join(app.config.apps_dir, f"{app.name}.git")
    pygit2.init_repository(bare=True, path=git_path)

    try:
      hook_path = str(os.path.join(git_path, "hooks", "post-receive"))
      hook = open(hook_path, "w")
      hook.write(Git.get_commit_hook(app))
      os.chmod(hook_path, stat.S_IRUSR|stat.S_IWUSR|stat.S_IXUSR)
    except Exception as e:
      raise(e)

    clonedir = tempfile.mkdtemp()
    repository = pygit2.clone_repository(url=git_path, path=clonedir)
    Git.setup_template(app, str(clonedir))
    user_name = "ownr"
    user_mail = "info@ownr.io"
    index = repository.index
    index.add_all()
    index.write()
    tree = index.write_tree()
    author = pygit2.Signature(user_name, user_mail)
    commiter = pygit2.Signature(user_name, user_mail)
    repository.create_commit("refs/heads/master", author, commiter, 
                             "initial commit",tree,[])
    remote = repository.remotes["origin"]
    remote.push(["refs/heads/master"])
    shutil.rmtree(clonedir)


  @staticmethod
  def format_commit(commit):
    tzinfo  = timezone( timedelta(minutes=commit.author.offset) )
    dt      = datetime.fromtimestamp(float(commit.author.time), tzinfo)
    return {"id": commit.hex, 
            "author": commit.author.name,
            "message": commit.message,
            "date": dt.strftime('%Y-%m-%d')
            }


  @staticmethod
  def get_commits(app):
    repo = Git.get_repository(app)
    last = repo[repo.head.target]
    return [Git.format_commit(commit) for commit in repo.walk(last.id, pygit2.GIT_SORT_TIME)]


  @staticmethod
  def get_readme(app):
    repo = Git.get_repository(app)
    last = repo[repo.head.target]
    for e in last.tree:
      if e.name.lower() == "readme.md":
        return repo.get(e.id).data
    
    return False


