from ruamel.yaml.compat import StringIO
from ruamel.yaml import YAML
import argparse


class StringYAML(YAML):
  def dump(self, data, stream=None, **kw):
    inefficient = False
    if stream is None:
      inefficient = True
      stream = StringIO()
    YAML.dump(self, data, stream, **kw)
    if inefficient:
      return stream.getvalue()

def get_args(args):
  if isinstance(args, argparse.Namespace):
    return vars(args)

  if isinstance(args, dict):
    return args

  raise TypeError("Args must be an instanse of argparse.Namespace or dict!")