from abc import abstractmethod
import docker
import logging
import kubernetes
import urllib3
from yaspin import yaspin
from ..utils import StringYAML

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

logger = logging.getLogger(__name__)


class Service():
  def __init__(self, config, name, version, image_name=None, replicas=1):
    self.config = config    
    self.name = name
    self.image_name = image_name or name    
    self.replicas = int(replicas)
    self.version = version
    self.image = f"{config.license}.ownr.io/{self.image_name}:{version}"
    self.network_name = f"{config.stack_name}_intranet"
    self.networks = [{'Target': self.network_name, 'Aliases': [self.name, self.name.lower()]}]    
    self.mode = {'Replicated': {'Replicas': self.replicas}}


  @property
  def service(self):
    if self.config.k8s_token:
      configuration = kubernetes.client.Configuration()
      configuration.api_key["authorization"] = self.config.k8s_token
      configuration.api_key_prefix['authorization'] = 'Bearer'
      configuration.verify_ssl = False
      configuration.host = self.config.k8s_host
      api_instance = kubernetes.client.AppsV1Api(kubernetes.client.ApiClient(configuration))

      try:
        return api_instance.read_namespaced_deployment(self.name, "default")
      except Exception:
        return None
    
    try:
      return self.config.engine.services.get(f"{self.config.stack_name}_{self.name}")
    except docker.errors.NotFound as e:
      logger.warn(f"Service {self.config.stack_name}_{self.name} not found")
      return None
    except (docker.errors.APIError, docker.errors.InvalidVersion) as e:
      logger.error(f"Service {self.name} [init]: {str(e)}")
      raise(e)


  @property
  def is_running(self):
    return not self.service is None


  def get_config(self):
    return {      
      "name": self.name,
      "version": self.version,
      "image_name": self.image_name, 
      "image": self.image,
      "network_name": f"{self.config.stack_name}_intranet",
      "networks": self.networks,
      "mode": self.mode
    }

  def download(self):
    with yaspin(text=f"Downloading {self.name}:{self.version} ...") as spinner:
      try:
        self.config.engine.images.pull(self.image)
      except docker.errors.APIError as e:
        logger.error(f"Error downloading service {self.name} [{self.image}]: {str(e)}")
        exit(1)
    spinner.write(f"> {self.name}:{self.version} downloaded.")


  def start_k8s(self):
    configuration = kubernetes.client.Configuration()
    configuration.api_key["authorization"] = self.config.k8s_token
    configuration.api_key_prefix['authorization'] = 'Bearer'
    configuration.verify_ssl = False
    configuration.host = self.config.k8s_host
    apps_api = kubernetes.client.AppsV1Api(kubernetes.client.ApiClient(configuration))
    core_api = kubernetes.client.CoreV1Api(kubernetes.client.ApiClient(configuration))
    
    claim = self.k8s_volume_claim()
    if claim:      
      logger.info(f"Create persistent mount claims for [{self.name}:{self.version}]")
      try: 
        core_api.create_namespaced_persistent_volume_claim("default", body=claim, pretty=True)
      except kubernetes.client.rest.ApiException as e:
        if e.reason == "Conflict":
          logger.warn(f"Persistent mount claims for [{self.name}:{self.version}] already existing, nothing to do")
        else:
          raise(e)
    

    logger.info(f"Starting kubernetes service [{self.name}:{self.version}]")
    try:
      core_api.create_namespaced_service("default", body=self.k8s_service(), pretty=True)
    except kubernetes.client.rest.ApiException as e:
      if e.reason == "Conflict":
        logger.warn(f"Kubernetes service [{self.name}:{self.version}] already exists, nothing to do")
      else:
        raise(e)


    logger.info(f"Starting kubernetes deployment [{self.name}:{self.version}]")
    try:      
      apps_api.create_namespaced_deployment("default", body=self.k8s_deployment(), pretty=True)
    except kubernetes.client.rest.ApiException as e:
      if e.reason == "Conflict":
        logger.warn(f"Kubernetes deployment [{self.name}:{self.version}] already exists, nothing to do")
      else:
        raise(e)


  def start_docker(self, download=True):
    if download:
      self.download()

    if self.is_running:
      logger.info(f"{type(self).__name__} {self.name} [start]: already running - nothing to do")    
      return
    
    try:
      self.config.engine.networks.get(self.network_name)
    except docker.errors.NotFound:
      try:
        self.config.engine.networks.create(name=self.network_name,
                                    driver="overlay",
                                    attachable=True,
                                    scope="swarm",
                                    labels={"com.docker.stack.namespace": self.config.stack_name})
        logger.info(f"Network {self.network_name} [create]: created")
      except docker.errors.APIError as e:
        logger.error(f"Network {self.network_name} [create]: {str(e)}")

                              
    try:
      app_config = self.get_config()
      app_config.pop("image_name")
      app_config.pop("network_name")
      app_config.pop("version")
      app_config["name"] = f"{self.config.stack_name}_{self.name}"
      self.config.engine.services.create(
        **app_config
      )
      logger.info(f"{type(self).__name__} {self.name} [start]: started")    
    except docker.errors.APIError as e:
      logger.error(f"{type(self).__name__} {self.name} [start]: {str(e)}")
      raise(e)


  def start(self, download=True):
    return self.start_k8s() if self.config.k8s_token \
      else self.start_docker(download=download)
    

  def stop_k8s(self):
    configuration = kubernetes.client.Configuration()
    configuration.api_key["authorization"] = self.config.k8s_token
    configuration.api_key_prefix['authorization'] = 'Bearer'
    configuration.verify_ssl = False
    configuration.host = self.config.k8s_host
    apps_api = kubernetes.client.AppsV1Api(kubernetes.client.ApiClient(configuration))
    try:
      apps_api.delete_namespaced_deployment(self.name, "default")
      logger.info(f"K8s deployment stop [{self.name}]")
    except Exception as e:
      logger.warn(f"Failed to stop k8s deployment [{self.name}]: {e}")


    core_api = kubernetes.client.CoreV1Api(kubernetes.client.ApiClient(configuration))
    try:
      core_api.delete_namespaced_service(self.name, "default")
      logger.info(f"K8s service stop [{self.name}]")
    except Exception as e:
      logger.warn(f"Failed to stop k8s deployment [{self.name}]: {e}")
      

  def stop_docker(self):
    if not self.is_running:
      logger.info(f"{type(self).__name__} {self.name} [stop]: not running - nothing to do")
      return

    try:
      self.service.remove()
      logger.info(f"{type(self).__name__} {self.name} [stop]: stopped")
    except docker.errors.APIError as e:
      logger.error(f"{type(self).__name__} {self.name} [stop]: {str(e)}")
      raise(e)



  def stop(self):
    return self.stop_k8s() if self.config.k8s_token \
      else self.stop_docker()
    

  def restart_k8s(self):
    pass


  def restart_docker(self):
    if not self.is_running:
      logger.info(f"{type(self).__name__} {self.name} [restart]: not running - starting the service")
      self.start()
      return

    try:
      self.service.reload(self.name)
      logger.info(f"{type(self).__name__} {self.name} [restart]: reloaded")
    except docker.errors.APIError as e:
      logger.error(f"{type(self).__name__} {self.name} [restart]: {str(e)}")
      raise(e)

    
  def restart(self):
    return self.restart_k8s() if self.config.k8s_token \
      else self.restart_docker()
    

  def scale(self, replicas):
    self.replicas = replicas
    if not self.is_running:
      logger.info(f"{type(self).__name__} {self.name} [stop]: not running - starting the service")      
      self.start()
      return

    try:
      self.service.scale(replicas)
      logger.info(f"{type(self).__name__} {self.name} [scale]: rescaled")
    except Exception as e:
      logger.error(f"{type(self).__name__} {self.name} [scale]: {str(e)}")
      raise(e)


  def logs(self, since=0, tail="all", details=False):
    if not self.is_running:
      logger.info(f"{type(self).__name__} {self.name} [logs]: not running- no logs")      
      self.start()
      return ""

    try:
      return self.service.logs(details=details,
                               stdout=True,
                               stderr=True,
                               since=since,
                               timestamps=True,
                               tail=tail)
    except Exception as e:
      logger.info(f"{type(self).__name__} {self.name} [error]: {str(e)}")


  def to_yaml(self):
    return StringYAML().dump(self.to_yaml())


  #@abstractmethod
  def k8s_volume_claim(self):
    return False


  #@abstractmethod
  def k8s_deployment(self):
    pass


  #@abstractmethod
  def k8s_service(self):
    pass