from ..service import Service

class Logger(Service):
  def __init__(self, app):
    super().__init__(app.config, name=f"{app.name}-log", image_name="log-collector",
                     version=app.config.major_version)

    self.constraints = [f"node.labels.ownr == {app.target}"] if app.target else []
    self.env = [f"EXPOSED_APP={app.name}"]
    self.app = app
    self.k8s_name = f"{app.name.replace('_', '-')}-log"
    self.k8s_name = self.k8s_name.lower()


  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["env"] = self.env

    return config


  def k8s_deployment(self):
    return {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
        "name": self.k8s_name,
        "labels": {
          "app": self.k8s_name
        }
      },
      "spec": {
        "replicas": 1,
        "selector": {
          "matchLabels": {
            "app": self.k8s_name
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "app": self.k8s_name
            }
          },
          "spec": {
            "containers": [
              {
                "name": self.k8s_name,
                "imagePullPolicy": "Always",
                "image": self.image,
                "ports": [
                  {
                    "containerPort": 514
                  },
                  {
                    "containerPort": 7777
                  }
                ],
                "env": [
                  {
                    "name": "EXPOSED_APP",
                    "value": self.app.name
                  }
                ]
              }
            ]
          }
        }
      }
    }


  def k8s_service(self):
    return {
      "apiVersion": "v1",
      "kind": "Service",
      "metadata": {
        "name": self.k8s_name,
        "labels": {
          "app": self.k8s_name
        }
      },
      "spec": {
        "type": "ClusterIP",
        "ports": [
          {
            "name": "syslogng",
            "port": 514,
            "targetPort": 514
          },
          {
            "name": "tailon",
            "port": 7777,
            "targetPort": 7777
          }
        ],
        "selector": {
          "app": self.k8s_name
        }
      }
    }

