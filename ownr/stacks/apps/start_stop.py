from .app_main import AppMain
from .app_worker import AppWorker
from .logger import Logger

import logging
logger = logging.getLogger(__name__)

def start_app(app):
  Logger(app).start()
  logger.warn(AppMain(app).k8s_service())
  AppMain(app).start()
  if app.app_type == "API":
    AppWorker(app).start()


def stop_app(app):
  Logger(app).stop()
  AppMain(app).stop()
  if app.app_type == "API":
    AppWorker(app).stop()


def restart_app(app):
  Logger(app).restart()
  AppMain(app).restart()
  if app.app_type == "API":
    AppWorker(app).restart()


def rescale_app(app):
  if app.app_type == "API":
    AppWorker(app).scale(int(app.replicas))
  else:
    AppMain(app).scale(int(app.replicas))


def services(app):
  isRunning = {
    "main": AppMain(app).is_running,
    "logger": Logger(app).is_running,
    "worker": True
  }

  if app.app_type == "API":
    isRunning["worker"] = AppWorker(app).is_running

  return isRunning

