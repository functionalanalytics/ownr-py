import hashlib
from ..service import Service


class AppMain(Service):
  def __init__(self, app):
    super().__init__(app.config,
                     name=app.name,
                     image_name=f"executor-{app.language.lower()}-{app.language_version}",
                     version=app.config.executor_version,
                     replicas=1 if app.app_type == "API" else int(app.replicas))

    self.constraints = [f"node.labels.ownr == {app.target}"] if app.target else []
    self.env = [f"EXPOSED_APP={app.name}",
                f"APP_TYPE={app.app_type}",
                f"VERSION={app.deploy_version}",
                f"DIGEST={hashlib.md5(app.config.license.encode()).hexdigest()}"]

    self.mounts = []
    self.app = app
    self.k8s_name = app.name.replace('_', '-')
    self.k8s_name = self.k8s_name.lower()

    if len(app.env):
      for env in app.env:
        self.env.append(f"{env.get('key')}={env.get('value')}")

    if len(app.mount):
      for mount in app.mount:
        self.mounts.append(f"{mount.get('key')}:{mount.get('value')}")

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["env"] = self.env
    if len(self.mounts):
      config["mounts"] = self.mounts

    return config


  def k8s_deployment(self):
    return {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
        "name": self.k8s_name,
        "labels": {
          "app": self.k8s_name
        }
      },
      "spec": {
        "replicas": 1,
        "selector": {
          "matchLabels": {
            "app": self.k8s_name
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "app": self.k8s_name
            }
          },
          "spec": {
            "containers": [
              {
                "name": self.k8s_name,
                "imagePullPolicy": "Always",
                "image": self.image,
                "ports": [
                  {
                    "containerPort": 8081
                  },
                  {
                    "containerPort": 8080
                  }
                ],
                "env": [
                  {
                    "name": "K8S",
                    "value": "true"
                  },
                  {
                    "name": "EXPOSED_APP",
                    "value": self.name
                  },
                  {
                    "name": "APP_TYPE",
                    "value": self.app.app_type
                  },
                  {
                    "name": "VERSION",
                    "value": self.app.deploy_version
                  },
                  {
                    "name": "DIGEST",
                    "value": hashlib.md5(self.app.config.license.encode()).hexdigest()
                  }
                ]
              }
            ]
          }
        }
      }
    }


  def k8s_service(self):
    return {
      "apiVersion": "v1",
      "kind": "Service",
      "metadata": {
        "name": self.k8s_name,
        "labels": {
          "app": self.k8s_name
        }
      },
      "spec": {
        "type": "ClusterIP",
        "ports": [
          {
            "name": "web",
            "port": 8081,
            "targetPort": 8081
          },
          {
            "name": "api",
            "port": 8080,
            "targetPort": 8080
          }
        ],
        "selector": {
          "app": self.k8s_name
        }
      }
    }
