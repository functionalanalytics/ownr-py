import re
import os
from ..service import Service


class Notebook(Service):
  def __init__(self, config, user):
    username = re.sub('[\.|@]', '_', user)
    super().__init__(config, 
                     name=f"{username}_notebook",
                     image_name="notebook",
                     version=config.notebook_version,
                     replicas=1)

    self.user = user.split("@")[0]
    self.username = username
    self.volume_name = f"{config.stack_name}_{username}_notebook_volume"
    self.k8s_claim_name = self.volume_name.replace("_", "-").lower()
    self.k8s_name = self.name.replace("_", "-").lower()

    self.constraints = ["node.role == manager"]

    mount_point = f"{self.volume_name}"
    if config.mount_point:
      mount_point = str(os.path.join(config.mount_point, mount_point))
      os.makedirs(mount_point, exist_ok=True)

    self.mounts = [f"{mount_point}:/opt/ownr/shared"]
    self.env = [f"USER={self.user}",
                f"NB_USER={self.username}",
                f"APP=python"]
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["env"] = self.env
    config["mounts"] = self.mounts

    return config


  def k8s_volume_claim(self):
   return {
     "apiVersion": "v1",
     "kind": "PersistentVolumeClaim",
     "metadata": {
       "name": self.k8s_claim_name,
       "labels": {
         "app": self.k8s_claim_name
       }
     },
     "spec": {
       "accessModes": ["ReadWriteOnce"],
       "resources": {
         "requests": {
           "storage": "1Gi"
         }
       }
     }
   }

  
  def k8s_service(self):
    return {
      "apiVersion": "v1",
      "kind": "Service",
      "metadata": {
        "name": self.k8s_name,
        "labels": {
          "app": self.k8s_name
        }
      },
      "spec": {
        "type": "ClusterIP",
        "ports": [
          {
            "port": 8888,
            "targetPort": 8888
          }
        ],
        "selector": {
          "app": self.k8s_name
        }
      }      
    }


  def k8s_deployment(self):
    return {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
        "name": self.k8s_name,
        "labels": {
          "app": self.k8s_name
        }
      },
      "spec": {
        "replicas": 1,
        "selector": {
          "matchLabels": {
            "app": self.k8s_name
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "app": self.k8s_name
            }
          },
          "spec": {
            "volumes": [
              {
                "name": self.k8s_claim_name,
                "persistentVolumeClaim": {
                  "claimName": self.k8s_claim_name
                }      
              }
            ],
            "containers": [
              {
                "name": self.k8s_name,
                "imagePullPolicy": "Always",
                "image": self.image,
                "volumeMounts": [
                  {
                    "name": self.k8s_claim_name,
                    "storageClassName": "standard",
                    "mountPath": "/opt/ownr/shared"
                  }
                ],
                "ports": [
                  {
                    "containerPort": 8888
                  }
                ],                
                "env": [
                  {
                    "name": "USER",
                    "value": self.user
                  },
                  {
                    "name": "NB_USER",
                    "value": self.username
                  },
                  {
                    "name": "APP",
                    "value": "python"
                  }
                ]
              }
            ]
          }
        }
      }
    }