from ..service import Service

class CouchDB(Service):
  def __init__(self, config):
    super().__init__(config, name="db", image_name="couchdb", version=config.couchdb)
    
    self.constraints = ["node.role == manager"]
    self.mounts = [f"{self.config.stack_name}_{self.config.stack_name}_db:/opt/couchdb/data"]
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["mounts"] = self.mounts

    return config
