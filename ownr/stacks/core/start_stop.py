from .administration import Administration
from .portainer import Portainer
from .influx import Influx
from .grafana import Grafana
from .node_monitor import Nodemonitor
from .couchdb import CouchDB
from .redis import Redis
from .rabbitmq import Rabbitmq
from .auth import Auth
from .postgres import Postgres

def start_core_services(config):
  Administration(config).start()
  Portainer(config).start()
  Influx(config).start()
  Grafana(config).start()
  Nodemonitor(config).start()
  CouchDB(config).start()
  Redis(config).start()
  Rabbitmq(config).start()
  Auth(config).start()
  Postgres(config).start()
  


def stop_core_services(config):
  Administration(config).stop()
  Portainer(config).stop()
  Influx(config).stop()
  Grafana(config).stop()
  Nodemonitor(config).stop()
  CouchDB(config).stop()
  Redis(config).stop()
  Rabbitmq(config).stop()
  Auth(config).stop()
  Postgres(config).stop()


def restart_core_services(config):
  Administration(config).restart()
  Portainer(config).restart()
  Influx(config).restart()
  Grafana(config).restart()
  Nodemonitor(config).restart()
  CouchDB(config).restart()
  Redis(config).restart()
  Rabbitmq(config).restart()
  Auth(config).restart()
  Postgres(config).restart()