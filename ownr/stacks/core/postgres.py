import os
from ..service import Service

class Postgres(Service):
  def __init__(self, config):
    super().__init__(config, name="postgres", version=config.postgres)
    
    self.constraints = ["node.role == manager"]

    self.env = ["POSTGRES_DB=keycloak",
                f"POSTGRES_USER={config.postgres_user}",
                f"POSTGRES_PASSWORD={config.postgres_password}",
                "PGDATA=/var/lib/postgresql/data/pgdata"]
    
    mount_point = f"{self.config.stack_name}_ownr_authdb"
    if config.mount_point:
      mount_point = str(os.path.join(config.mount_point, mount_point))
      os.makedirs(mount_point, exist_ok=True)

    self.mounts = [f"{mount_point}:/var/lib/postgresql/data/pgdata"]
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["env"] = self.env
    config["mounts"] = self.mounts

    return config


  def k8s_volume_claim(self):
   return {
     "apiVersion": "v1",
     "kind": "PersistentVolumeClaim",
     "metadata": {
       "name": "authdb",
       "labels": {
         "app": "auth"
       }
     },
     "spec": {
       "accessModes": ["ReadWriteOnce"],
       "resources": {
         "requests": {
           "storage": "1Gi"
         }
       }
     }
   }


  def k8s_deployment(self):
    return {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
        "name": "authdb",
        "labels": {
          "app": "authdb"
        }
      },
      "spec": {
        "replicas": 1,
        "selector": {
          "matchLabels": {
            "app": "authdb"
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "app": "authdb"
            }
          },
          "spec": {
            "volumes": [
              {
                "name": "authdb",
                "persistentVolumeClaim": {
                  "claimName": "authdb"
                }      
              }
            ],
            "containers": [
              {
                "name": "postgres",
                "imagePullPolicy": "Always",
                "image": self.image,
                "volumeMounts": [
                  {
                    "name": "authdb",
                    "storageClassName": "standard",
                    "mountPath": "/var/lib/postgresql/data"
                  }
                ],
                "ports": [
                  {
                    "containerPort": 5432
                  }
                ],                
                "env": [
                  {
                    "name": "POSTGRES_DB",
                    "value": "keycloak"
                  },
                  {
                    "name": "POSTGRES_USER",
                    "value": self.config.postgres_user
                  },
                  {
                    "name": "POSTGRES_PASSWORD",
                    "value": self.config.postgres_password
                  },
                  { 
                    "name": "PGDATA",
                    "value": "/var/lib/postgresql/data/pgdata"
                  }
                ]
              }
            ]
          }
        }
      }
    }


  def k8s_service(self):
    return {
      "apiVersion": "v1",
      "kind": "Service",
      "metadata": {
        "name": "authdb",
        "labels": {
          "app": "authdb"
        }
      },
      "spec": {
        "type": "ClusterIP",
        "ports": [
          {
            "port": 5432,
            "targetPort": 5432
          }
        ],
        "selector": {
          "app": "authdb"
        }
      }      
    }

