import requests
import os
import json
from ..service import Service
from .influx import Influx

class Grafana(Service):

  def __init__(self, config):
    super().__init__(config, name="grafana", image_name="monitor", version=config.major_version)

    mount_point = f"{self.config.stack_name}_{self.config.stack_name}_grafana"
    if config.mount_point:
      mount_point = str(os.path.join(config.mount_point, mount_point))
      os.makedirs(mount_point, exist_ok=True)
      os.chmod(mount_point, mode=0o777)
    

    self.constraints = ["node.role == manager"]
    self.mounts = [f"{mount_point}:/var/lib/grafana"]

    self.env = ["GF_SERVER_ROOT_URL=/deployed/utils/monitor/"]
  

  def get_datasource(self):
    return {
      "name":"influx",
      "type":"influxdb",
      "url": f"http://{Influx.name}:{Influx.port}",
      "access":"proxy",
      "database": "ownr"
    }


  def get_prefs(self):
    return {
      "theme": "",
      "homeDashboardId": 1,
      "timezone":"utc"
    }


  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["mounts"] = self.mounts
    config["env"] = self.env
    return config

  
  def setup(self):
    conf_dir = str(os.path.join(os.path.dirname(__file__), "conf"))    
    requests.post("http://grafana:3000/api/datasources",    
                  auth=requests.auth.HTTPBasicAuth('admin', 'admin'),
                  data=json.dumps(self.get_datasource()),
                  headers={
                    "Content-Type": "application/json"
                  })

    requests.post("http://grafana:3000/api/dashboards/db",      
                  auth=requests.auth.HTTPBasicAuth('admin', 'admin'),                
                  data=open(os.path.join(conf_dir, "dashboard.json"), 'rb'),
                  headers={
                    "Content-Type": "application/json"
                  })
    
    requests.put("http://grafana:3000/api/org/preferences",       
                 auth=requests.auth.HTTPBasicAuth('admin', 'admin'),               
                 data=json.dumps(self.get_prefs()),
                 headers={
                   "Content-Type": "application/json"
                 })
