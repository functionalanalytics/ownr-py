import json
from ..service import Service
from ...keycloak import Keycloak

class Auth(Service):

  def __init__(self, config):
    super().__init__(config, name="auth", image_name="keycloak", version=config.keycloak)
    
    self.constraints = ["node.role == manager"]
    
    self.env = [f"KEYCLOAK_PASSWORD={config.keycloak_password}",
                "PROXY_ADDRESS_FORWARDING=true",
                f"KEYCLOAK_USER={config.keycloak_user}",
                f"POSTGRES_USER={config.postgres_user}",
                f"POSTGRES_PASSWORD={config.postgres_password}",
                "DB_VENDOR=POSTGRES"]


  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["env"] = self.env

    return config

    
  def setup(self):
    kc = Keycloak(self.config.keycloak_user, self.config.keycloak_password, "http://auth:8080")
    kc.setup_admin_client(self.config.hostname)
    kc.create_realm("ownr", self.config.platform_name)
    kc.create_user(username="ownr-admin", realm="ownr", enabled = True, password = "ownr-admin")
    kc.create_user(username="ownr-internal", realm="ownr", enabled = False)
    kc.create_role("ownr-admin", "ownR Platform Administrator", "ownr")
    kc.create_role("ownr-dev", "ownR app developer", "ownr")
    kc.add_role_to_user("ownr-admin", kc.get_user_id("ownr-admin", "ownr"), "ownr")
    kc.create_platform_client(self.config.hostname, "ownr")


  def k8s_deployment(self):
    return {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
        "name": "auth",
        "labels": {
          "app": "auth"
        }
      },
      "spec": {
        "replicas": 1,
        "selector": {
          "matchLabels": {
            "app": "auth"
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "app": "auth"
            }
          },
          "spec": {
            "containers": [
              {
                "name": "keycloak",
                "imagePullPolicy": "Always",
                "image": self.image,
                "ports": [
                  {
                    "containerPort": 8080
                  }
                ],                
                "env": [
                  {
                    "name": "DB_ADDR",
                    "value": "authdb"
                  },
                  {
                    "name": "DB_PORT",
                    "value": "5432"
                  },
                  {
                    "name": "POSTGRES_USER",
                    "value": self.config.postgres_user
                  },
                  {
                    "name": "POSTGRES_PASSWORD",
                    "value": self.config.postgres_password
                  },
                  {
                    "name": "DB_VENDOR",
                    "value": "postgres"
                  },
                  {
                    "name": "KEYCLOAK_USER",
                    "value": self.config.keycloak_user
                  },
                  {
                    "name": "KEYCLOAK_PASSWORD",
                    "value": self.config.keycloak_password
                  },
                  {
                    "name": "KEYCLOAK_FRONTEND_URL",
                    "value": f"{self.config.hostname}/auth"
                  },
                  {
                    "name": "PROXY_ADDRESS_FORWARDING",
                    "value": "true"
                  }
                ]
              }
            ]
          }
        }
      }
    }
    

  def k8s_service(self):
    return {
      "apiVersion": "v1",
      "kind": "Service",
      "metadata": {
        "name": "auth",
        "labels": {
          "app": "auth"
        }
      },
      "spec": {
        "type": "ClusterIP",
        "ports": [
          {
            "port": 8080,
            "targetPort": 8080
          }
        ],
        "selector": {
          "app": "auth"
        }
      }      
    }
