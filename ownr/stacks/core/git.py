from ..service import Service

class Git(Service):
  def __init__(self, config):
    super().__init__(config, name="git", version=config.major_version)
    
    self.constraints = ["node.role == manager"]
    self.mounts = ["/var/run/docker.sock:/var/run/docker.sock",
                   f"{self.config.stack_name}_{self.config.stack_name}_git:/opt/ownr/git"]
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["mounts"] = self.mounts

    return config
