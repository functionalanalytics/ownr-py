from ..service import Service

class Rabbitmq(Service):
  def __init__(self, config):
    super().__init__(config, name="rabbitmq", version=config.rabbitmq)
    
    self.constraints = ["node.role == manager"]    
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    return config

  
  def k8s_service(self):
    return {
      "apiVersion": "v1",
      "kind": "Service",
      "metadata": {
        "name": "rabbitmq",
        "labels": {
          "app": "rabbitmq"
        }
      },
      "spec": {
        "type": "ClusterIP",
        "ports": [
          {
            "name": "default",
            "port": 5672,
            "targetPort": 5672
          },
          {
            "name": "management",
            "port": 15672,
            "targetPort": 15672
          }
        ],
        "selector": {
          "app": "rabbitmq"
        }
      }      
    }

    
  def k8s_deployment(self):
    return {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
        "name": "rabbitmq",
        "labels": {
          "app": "rabbitmq"
        }
      },
      "spec": {
        "replicas": 1,
        "selector": {
          "matchLabels": {
            "app": "rabbitmq"
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "app": "rabbitmq"
            }
          },
          "spec": {
            "containers": [
              {
                "name": "rabbitmq",
                "imagePullPolicy": "Always",
                "image": self.image,
                "ports": [
                  {
                    "containerPort": 5672
                  },
                  {
                    "containerPort": 15672
                  }
                ]
              }
            ]
          }
        }
      }
    }
