import os
from ..service import Service

class Redis(Service):
  def __init__(self, config):
    super().__init__(config, name="redis", version=config.redis)
    
    self.constraints = ["node.role == manager"]

    mount_point = f"{self.config.stack_name}_{self.config.stack_name}_redis"
    if config.mount_point:
      mount_point = str(os.path.join(config.mount_point, mount_point))
      os.makedirs(mount_point, exist_ok=True)

    self.mounts = [f"{mount_point}:/data"]
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["mounts"] = self.mounts

    return config


  def k8s_service(self):
    return {
      "apiVersion": "v1",
      "kind": "Service",
      "metadata": {
        "name": "redis",
        "labels": {
          "app": "redis"
        }
      },
      "spec": {
        "type": "ClusterIP",
        "ports": [
          {
            "port": 6379,
            "targetPort": 6379
          }
        ],
        "selector": {
          "app": "redis"
        }
      }      
    }

    
  def k8s_deployment(self):
    return {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
        "name": "redis",
        "labels": {
          "app": "redis"
        }
      },
      "spec": {
        "replicas": 1,
        "selector": {
          "matchLabels": {
            "app": "redis"
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "app": "redis"
            }
          },
          "spec": {
            "containers": [
              {
                "name": "redis",
                "imagePullPolicy": "Always",
                "image": self.image,
                "ports": [
                  {
                    "containerPort": 6379
                  }
                ]
              }
            ]
          }
        }
      }
    }