from ..service import Service

class Portainer(Service):
  def __init__(self, config):
    super().__init__(config, name="administration", image_name="portainer", version=config.major_version)
    
    self.constraints = ["node.role == manager"]    
    self.mounts = ["/var/run/docker.sock:/var/run/docker.sock"]
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["mounts"] = self.mounts

    return config
