from ..service import Service

class Nodemonitor(Service):
  def __init__(self, config):
    super().__init__(config, name="node-monitor", image_name="cadvisor", 
                     version=config.cadvisor)
    
    self.constraints = ["node.role == manager"]
    self.args = ["-logtostderr", "-docker_only", "-storage_driver=influxdb", 
                 "-storage_driver_db=ownr", "-storage_driver_host=influx:8086"]
    self.mounts = ["/:/rootfs:ro",
                   "/var/run:/var/run:rw",
                   "/sys:/sys:ro",
                   "/var/lib/docker/:/var/lib/docker:ro"]
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["args"] = self.args
    config["mounts"] = self.mounts
    config["hostname"] = '{{.Node.Hostname}}'
    config["mode"] = "global"

    return config
