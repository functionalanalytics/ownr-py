import os
from ..service import Service
from docker.types import EndpointSpec

class Administration(Service):
  def __init__(self, config):
    super().__init__(config, name="admin", image_name="admin-flask", version=config.major_version)
    self.ports = {}
    self.ports[self.config.port] = 80
    
    self.constraints = ["node.role == manager"]
    self.endpoint = EndpointSpec(ports=self.ports)

    self.mounts = ["/:/rootfs:ro",
                   "/var/run:/var/run:rw",
                   "/sys:/sys:ro",
                   "/var/lib/docker/:/var/lib/docker:ro",
                   f"{str(os.path.expanduser(self.config.config_dir))}:/root/.ownr/"]

    if self.config.mount_point:
      self.mounts.append(f"{self.config.mount_point}:{self.config.mount_point}")

    if not self.config.secure_docker:
      self.mounts.append("/var/run/docker.sock:/var/run/docker.sock")
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["endpoint_spec"] = self.endpoint
    config["mounts"] = self.mounts

    return config


  def k8s_service(self):
    return {
      "apiVersion": "v1",
      "kind": "Service",
      "metadata": {
        "name": "ownr-administration",
        "labels": {
          "app": "ownr"
        },
        "annotations": {
          "beta.cloud.google.com/backend-config": '{"ports": {"8080": "ownr-ingress-backend"}}'
        }
      },
      "spec": {
        "type": "NodePort",
        "ports": [
          {
            "port": 8080,
            "targetPort": 80
          }
        ],
        "selector": {
          "app": "ownr"
        }
      }      
    }
  

  def k8s_deployment(self):
    return {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
        "name": "ownr",
        "labels": {
          "app": "ownr"
        }
      },
      "spec": {
        "replicas": 1,
        "selector": {
          "matchLabels": {
            "app": "ownr"
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "app": "ownr"
            }
          },
          "spec": {
            "volumes": [
              {
                "name": "ownr-storage",
                "persistentVolumeClaim": {
                  "claimName": "ownr-storage"
                }      
              }
            ],
            "containers": [
              {
                "name": "admin",
                "imagePullPolicy": "Always",
                "image": f"{self.config.license}.ownr.io/admin-k8s:{self.version}",
                "volumeMounts": [
                  {
                    "name": "ownr-storage",
                    "storageClassName": "standard",
                    "mountPath": "/root/.ownr"
                  }
                ],
                "envFrom": [
                  {
                    "configMapRef": {
                      "name": "ownr-config"
                    }
                  }
                ],
                "ports": [
                  {
                    "containerPort": 80
                  }
                ]          
              },
              {
                "name": "dnsmasq",
                "image": "janeczku/go-dnsmasq:release-1.0.5",
                "args": [
                  "--listen",
                  "127.0.0.1:53",
                  "--default-resolver",
                  "--append-search-domains",
                  "--hostsfile=/etc/hosts",
                  "--verbose"
                ]
              }
            ]
          }
        }
      }
    }


  def k8s_volume_claim(self):
   return {
     "apiVersion": "v1",
     "kind": "PersistentVolumeClaim",
     "metadata": {
       "name": "ownr-storage",
       "labels": {
         "app": "ownr-storage"
       }
     },
     "spec": {
       "accessModes": ["ReadWriteOnce"],
       "resources": {
         "requests": {
           "storage": "1Gi"
         }
       }
     }
   }

  
    
