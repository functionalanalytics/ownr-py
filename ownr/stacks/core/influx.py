import influxdb
import os
from ..service import Service

class Influx(Service):
  port = 8086
  name = "influx"

  def __init__(self, config):
    super().__init__(config, name=Influx.name, image_name="influxdb", version=config.influxdb)
    
    self.constraints = ["node.role == manager"]

    mount_point = f"{self.config.stack_name}_{self.config.stack_name}_influx"
    if config.mount_point:
      mount_point = str(os.path.join(config.mount_point, mount_point))
      os.makedirs(mount_point, exist_ok=True)

    self.mounts = [f"{mount_point}:/var/lib/influxdb"]
  

  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["mounts"] = self.mounts
    return config


  def setup(self):
    influxdb.InfluxDBClient("influx", 8086).create_database("ownr")
