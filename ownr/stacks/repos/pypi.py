import logging
import os
from ..service import Service

logger = logging.getLogger(__name__)

class Pypi(Service):
  def __init__(self, repo): 
    super().__init__(repo.config, 
                     name=repo.name,
                     image_name="pypiserver",                      
                     version="v1.3.1",
                     replicas=1)

    self.constraints = ["node.role == manager"]
    self.volume_name = f"{repo.config.stack_name}_{repo.name}_repo_volume"
    self.k8s_claim_name = self.volume_name.replace("_", "-").lower()
    self.k8s_name = self.name.replace("_", "-").lower()

    mount_point = f"{self.config.stack_name}_{self.config.stack_name}_{self.name}"
    if self.config.mount_point:
      mount_point = str(os.path.join(self.config.mount_point, mount_point))
      os.makedirs(mount_point, exist_ok=True)

    self.mounts = [f"{mount_point}:/home/pypiserver/packages"]

  
  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["mounts"] = self.mounts

    return config


  def k8s_volume_claim(self):
   return {
     "apiVersion": "v1",
     "kind": "PersistentVolumeClaim",
     "metadata": {
       "name": self.k8s_claim_name,
       "labels": {
         "app": self.k8s_claim_name
       }
     },
     "spec": {
       "accessModes": ["ReadWriteOnce"],
       "resources": {
         "requests": {
           "storage": "1Gi"
         }
       }
     }
   }


  def k8s_service(self):
    return {
      "apiVersion": "v1",
      "kind": "Service",
      "metadata": {
        "name": self.k8s_name,
        "labels": {
          "app": self.k8s_name
        }
      },
      "spec": {
        "type": "ClusterIP",
        "ports": [
          {
            "port": 8082,
            "targetPort": 8082
          }
        ],
        "selector": {
          "app": self.k8s_name
        }
      }      
    }


  def k8s_deployment(self):
    return {
      "apiVersion": "apps/v1",
      "kind": "Deployment",
      "metadata": {
        "name": self.k8s_name,
        "labels": {
          "app": self.k8s_name
        }
      },
      "spec": {
        "replicas": 1,
        "selector": {
          "matchLabels": {
            "app": self.k8s_name
          }
        },
        "template": {
          "metadata": {
            "labels": {
              "app": self.k8s_name
            }
          },
          "spec": {
            "volumes": [
              {
                "name": self.k8s_claim_name,
                "persistentVolumeClaim": {
                  "claimName": self.k8s_claim_name
                }      
              }
            ],
            "containers": [
              {
                "name": self.k8s_name,
                "imagePullPolicy": "Always",
                "image": self.image,
                "volumeMounts": [
                  {
                    "name": self.k8s_claim_name,
                    "storageClassName": "standard",
                    "mountPath": "/home/pypiserver/packages"
                  }
                ],
                "ports": [
                  {
                    "containerPort": 8082
                  }
                ]
              }
            ]
          }
        }
      }
    }