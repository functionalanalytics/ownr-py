from ..service import Service
import logging
import os

logger = logging.getLogger(__name__)

class Registry(Service):
  def __init__(self, repo): 
    super().__init__(repo.config, 
                     name=repo.name,
                     image_name="docker-registry",
                     version="dev",
                     replicas=1)

    self.constraints = ["node.role == manager"]
    self.url = f"/deployed/web/{self.name}/"
    self.docker_url = f"/repo/{self.name}/"
    self.env = [f"RAILS_RELATIVE_URL_ROOT={self.url}/",
                f"SCRIPT_NAME={self.url}",
                f"PREFIX={self.docker_url}",
                f"DOCKER_REGISTRY_URL=http://localhost:8082{self.docker_url}"]

    mount_point = f"{self.config.stack_name}_{self.config.stack_name}_{self.name}"
    if repo.config.mount_point:
      mount_point = str(os.path.join(repo.config.mount_point, mount_point))
      os.makedirs(mount_point, exist_ok=True)
                
    self.mounts = [f"{mount_point}:/var/lib/registry"]


  def get_config(self):
    config = super().get_config()
    config["constraints"] = self.constraints
    config["env"] = self.env
    config["mounts"] = self.mounts

    return config