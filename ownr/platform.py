import os
import docker
import logging
import requests
import argparse
import subprocess
import time
from yaspin import yaspin
from .config import Config
from .stacks import core
from .stacks import start_core_services, stop_core_services, restart_core_services

logger = logging.getLogger(__name__)

class Platform():

  def __init__(self, args):
    if isinstance(args, argparse.Namespace):
      args = vars(args)


    if os.path.isfile(os.path.join(str(os.path.expanduser(args.get("config_dir", ""))), "ownr.yaml")):
      logger.warn("local config found")
      self.config = Config.from_yaml(args.get("config_dir"))
    else:
      logger.warn("config from env")
      self.config = Config(args={})
    
    logger.warn(not self.config.k8s_token)

    if not self.config.k8s_token:
      logger.warn("docker config")
      try:
        self.config.engine.ping()
      except (docker.errors.APIError, requests.exceptions.ConnectionError):
        logger.error("Could not connect to docker!")
        exit(1)
      logger.debug("Connected to docker.")
  

  @staticmethod
  def is_running(stack_name):
    docker_script = subprocess.Popen(["docker", "stack", "ls", "--format", "{{.Name}}"], stdout=subprocess.PIPE)
    grep_script  = subprocess.Popen(["grep", stack_name], stdin=docker_script.stdout, stdout=subprocess.PIPE)
    return bool(grep_script.communicate()[0])
  
  
  @staticmethod
  def get_parser(management_commands):
    parser = management_commands.add_parser("platform", help="manage the ownr platform")
    parser._positionals.title = argparse.SUPPRESS
    platform_commands = parser.add_subparsers(help="", metavar="Command", dest="subcommand")
    
    start_command = platform_commands.add_parser("start", help="Start the ownr platform")
    start_command.add_argument("--no-swarm", action="store_true", help="Do not start swarm mode if not running")
    start_command.add_argument("--download-images", action="store_true", help="Download platform images")
    start_command.add_argument("--service", help="service to start")
    start_command.add_argument("--setup", action="store_true", help="setup an already running service")


    stop_command = platform_commands.add_parser("stop", help="Stop the ownr platform")
    stop_command.add_argument("--no-swarm", action="store_true", help="Do not turn off docker's swarm mode")
    stop_command.add_argument("--keep-apps", action="store_false", help="Do not stop the applications")
    stop_command.add_argument("--service", help="service to stop")


    setup_command = platform_commands.add_parser("setup", help="Setup a service")
    setup_command.add_argument("--service", required=True, help="service to setup")
    return parser

  def download_service(self, ownr_license, service, version):
    with yaspin(text=f"Downloading {service}:{version} ...") as spinner:
      try:
        self.config.engine.images.pull(f"{ownr_license}.ownr.io/{service}:{version}")
      except docker.errors.APIError as e:
        logger.error(f"Error downloading service {service}: {str(e)}")
        exit(1)
    spinner.write(f"> {service}:{version} downloaded.")


  def start(self, args):
    return self.start_k8s(args) if self.config.k8s_token \
      else self.start_docker(args)


  def start_k8s(self, args):   
    logger.warn("k8s start")
    if not hasattr(core, args.service.capitalize()):
        logger.error(f"{args.service}: no such service")
        exit(1)        

    service = getattr(core, args.service.capitalize())
    service(self.config).start(args.download_images)


  def start_docker(self, args):    
    try:
      self.config.engine.nodes.list()
      logger.debug("Docker swarm mode is on.")
    except docker.errors.APIError:
      if args.no_swarm:
        logger.error("Docker isn't running in swarm mode, the platform cannot be started!")
        exit(1)
      logger.info("Docker isn't running in swarm mode, turning it on ...")
      self.config.engine.swarm.init()
      logger.info("Docker swarm mode is on.")


    if args.service:
      if not hasattr(core, args.service.capitalize()):
        logger.error(f"{args.service}: no such service")
        exit(1)        

      service = getattr(core, args.service.capitalize())
      service(self.config).start(args.download_images)
      return 

    if args.download_images:
      logger.info("Downloading services ...")
      self.download_service(self.config.license, "portainer", self.config.major_version)
      self.download_service(self.config.license, "monitor", self.config.major_version)
      self.download_service(self.config.license, "admin", self.config.major_version)
      self.download_service(self.config.license, "git", self.config.major_version)
      self.download_service(self.config.license, "log-collector", self.config.major_version)   

      self.download_service(self.config.license, "influxdb", self.config.influxdb)
      self.download_service(self.config.license, "cadvisor", self.config.cadvisor)
      self.download_service(self.config.license, "couchdb", self.config.couchdb)
      self.download_service(self.config.license, "redis", self.config.redis)
      self.download_service(self.config.license, "rabbitmq", self.config.rabbitmq)
      self.download_service(self.config.license, "keycloak", self.config.keycloak)
      self.download_service(self.config.license, "postgres", self.config.postgres)

      for version in self.config.python_versions:
        self.download_service(self.config.license, f"executor-python-{version}", self.config.executor_version)

      for version in self.config.R_versions:
        self.download_service(self.config.license, f"executor-r-{version}", self.config.executor_version)

      self.download_service(self.config.license, "notebook", self.config.notebook_version)

      if not self.config.disable_ide:
        self.download_service(self.config.license, "ide", self.config.ide_version)

    logger.info("Deploying core services ...")

    start_core_services(self.config)

    logger.info("The platform is up and running.")


  def setup(self, args):
    if not hasattr(core, args.service.capitalize()):
      logger.error(f"{args.service}: no such service")
      exit(1)        

    service = getattr(core, args.service.capitalize())

    if not hasattr(service, "setup"):
      logger.error(f"{args.service}: no setup step")
      exit(1)

    service(self.config).setup()
    return


  def stop(self, args):
    try:
      self.config.engine.nodes.list()
    except docker.errors.APIError:
      logger.error("Docker isn't running in swarm mode, the platform is not running.")
      exit(1)

    if args.service:
      if not hasattr(core, args.service.capitalize()):
        logger.error(f"{args.service}: no such service")
        exit(1)        

      service = getattr(core, args.service.capitalize())
      service(self.config).stop()
      return


    logger.info("Removing core services ...")
    stop_core_services(self.config)
    

    if args.keep_apps:
      logger.info("Removing applications ...")
      ownr_services = self.config.engine.services.list(filters={"name": f"{self.config.stack_name}_"})
      for service in ownr_services:
        logger.info(f"Removing app {service.name} ...")
        service.remove()

      network_exists = True
      with yaspin(text=f"Removing network {self.config.stack_name}_intranet ..."):
        while network_exists:
          try:
            network = self.config.engine.networks.get(f"{self.config.stack_name}_intranet")
            network.remove()
            time.sleep(1)          
          except docker.errors.NotFound:
            network_exists = False
          except docker.errors.APIError:
            time.sleep(1)
    
    if args.no_swarm:
      logger.info("Stopping swarm mode ...")
      self.config.engine.swarm.leave(force=True)
      logger.info("Done.")


