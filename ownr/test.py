import pygit2, tempfile

pygit2.init_repository("/tmp/lofasz", bare=True)
clonedir = str(tempfile.mkdtemp())
repo = pygit2.clone_repository("/tmp/lofasz", clonedir)