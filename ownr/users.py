import argparse
import logging
from shutil import Error
import uuid

from .keycloak import Keycloak
from .config import Config
from . import utils

logger = logging.getLogger(__name__)


class Users:

  
  @staticmethod
  def get_parser(management_commands):
    parser = management_commands.add_parser("users", help="Manage ownR users")
    parser._positionals.title = argparse.SUPPRESS

    commands = parser.add_subparsers(help="", metavar="Command", dest="subcommand")    
    list_command = commands.add_parser("list", help="list users (for a specific app, see --app)")
    list_command.add_argument("--app", help="name of the application")
    grant_command = commands.add_parser("grant", help="Grant access for a user to am app")
    grant_command.add_argument("--app", required="true", help="name of the application")
    grant_command.add_argument("--user", required="true", help="name of the user")
    revoke_command = commands.add_parser("revoke", help="Grant access for a user to am app")
    revoke_command.add_argument("--app", required="true", help="name of the application")
    revoke_command.add_argument("--user", required="true", help="name of the user")
    service_account = commands.add_parser("serviceaccount", help="manage service accounts")
    service_account.add_argument("--name", help="name of the service account")
    apikey = commands.add_parser("apikey", help="get ownr api key of an (service) account")
    apikey.add_argument("--name", help="name of the (service) account")
    return parser


  @staticmethod
  def list(args, format=True):
    args = utils.get_args(args)
    config = Config.from_yaml(args.get("config_dir"))
    kc = Keycloak(config.keycloak_user, 
                      config.keycloak_password, 
                      config.hostname)
    
    if not args.get("app", None):
      users = kc.get_all_users("ownr")
    else:
      users = kc.get_role_users(f"ownr_role_{args.get('app')}", "ownr")

    kc.logout()

    if not format:
      return users

    for user in users:
      print(user.get("username"))


  @staticmethod
  def grant(args):
    args = utils.get_args(args)
    config = Config.from_yaml(args.get("config_dir"))
    kc = Keycloak(config.keycloak_user, config.keycloak_password, config.hostname)
    userid = kc.get_user_id(args.get('user'), "ownr")
    kc.add_role_to_user(f"ownr_role_{args.get('app')}", userid, "ownr")
    kc.logout()


  @staticmethod
  def revoke(args):
    args = utils.get_args(args)
    config = Config.from_yaml(args.get("config_dir"))
    kc = Keycloak(config.keycloak_user, config.keycloak_password, config.hostname)
    userid = kc.get_user_id(args.get('user'), "ownr")
    kc.remove_role_from_user(f"ownr_role_{args.get('app')}", userid, "ownr")
    kc.logout()


  @staticmethod
  def impersonate(args):
    args = utils.get_args(args)
    config = Config.from_yaml(args.get("config_dir"))
    kc = Keycloak(config.keycloak_user, config.keycloak_password, config.hostname)
    userid = kc.get_user_id(args.get('user'), "ownr")
    return kc.impersonate(userid, "ownr")
    

  @staticmethod
  def serviceaccount(args):
    args = utils.get_args(args)
    config = Config.from_yaml(args.get("config_dir"))
    kc = Keycloak(config.keycloak_user, config.keycloak_password, config.hostname)
    kc.create_user(username=args.get("name"), realm="ownr", enabled = False)
    token = str(uuid.uuid4())
    kc.set_user_platform_token(args.get("name"), "ownr", token)
    kc.add_role_to_user("ownr-dev", kc.get_user_id(args.get("name"), "ownr"), "ownr")
    logger.info(f"""Service account {args.get("name")} created with token
    {token}
    """)
    return token
    

  @staticmethod
  def apikey(args):
    try:
      args = utils.get_args(args)
      config = Config.from_yaml(args.get("config_dir"))
      kc = Keycloak(config.keycloak_user, config.keycloak_password, config.hostname)
      user_info = kc.get_user_info(kc.get_user_id(args.get("name"), "ownr"), "ownr")
      print(user_info.get("attributes", {}).get("ownr-platform-key", [""])[0])
    except Exception:
      print(f"User {args.get('name')} not found.")
