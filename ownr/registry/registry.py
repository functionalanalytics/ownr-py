import re
from dxf import DXFBase, DXF
from urllib.parse import urlparse
from functools import lru_cache

class RegistryV2:
  def __init__(self, conf):
    self.host = urlparse(conf.hostname).netloc


  @property
  @lru_cache()
  def registry(self):
    return DXFBase("test.ownr.io")  


  def get_namespaces(self):
    repos = self.registry.list_repos()
    r = re.compile('^repo/(.*)/.*')
    return list(set(
      filter(None.__ne__,
            [r.match(repo)[1] if not r.match(repo) is None else None for repo in repos])
    ))


  def get_repositories(self, namespace):
    repos = self.registry.list_repos()
    r = re.compile(f'^repo/{namespace}/(.*)')
    return list(
      filter(None.__ne__,
            [r.match(repo)[1] if not r.match(repo) is None else None for repo in repos])
    )


  def get_tags(self, namespace, repository):
    repo = DXF(self.host, f"repo/{namespace}/{repository}")    
    return repo.list_aliases()


  def del_tag(self, namespace, repository, tag):
    repo = DXF(self.host, f"repo/{namespace}/{repository}")    
    blobs = repo.del_alias(tag)
    
    for blob in blobs:
      repo.del_blob(blob)
