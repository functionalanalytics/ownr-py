import argparse
import logging
import os
import shutil

from .config import Config
from .keycloak import Keycloak
from .platform import Platform
from .utils import StringYAML
from .stacks.apps import start_app, stop_app, restart_app, rescale_app
from .git import Git
from ruamel.yaml import YAML

logger = logging.getLogger(__name__)


class Repository():
  def __init__(self, args, default_config = {}):
    if isinstance(args, argparse.Namespace):
      args = vars(args)

    self.config = Config.from_yaml(args.get("config_dir"))

    self.name = args.get("name") or default_config.get("name" , "")
    self.type = args.get("type") or default_config.get("type", "")
    self.description = args.get("description") or default_config.get("description", "")    


  @property
  def stack_name(self):
    return f"{self.config.stack_name}_app_{self.name}"


  def to_dict(self):
    return {"version": 1, 
            "name": self.name,
            "type": self.type,
            "description": self.description,
    }


  def to_yaml(self):
    return StringYAML().dump(self.to_dict())  


  @staticmethod
  def from_yaml(yaml_file, config_dir):
    try:
      config_fp = open(yaml_file, "r")
      data = YAML().load(config_fp)    
      data["config_dir"] = config_dir
      config_fp.close()
    except Exception as e:
      raise e

    return Repository(data)


  @staticmethod
  def create(args):
    repo = Repository(args)
    Repository.save_repo(repo)


  @staticmethod
  def get_parser(management_commands):
    parser = management_commands.add_parser("repository", help="manage a repository running on the ownR platform")
    parser._positionals.title = argparse.SUPPRESS
    commands = parser.add_subparsers(help="", metavar="Command", dest="subcommand")
    
    create_command = commands.add_parser("create", help="create an application")
    create_command.add_argument("--name", required=True, help="name of the application")
    create_command.add_argument("--type", required=True, choices=["cran", "pypi", "registry", "git"], metavar="type", help="repository type")

    start_command = commands.add_parser("start", help="start a repository")
    start_command.add_argument("--name", required=True, help="name of the repository")

    stop_command = commands.add_parser("stop", help="stop a repository")
    stop_command.add_argument("--name", required=True, help="name of the repository")

    commands.add_parser("list", help="list all repositories")    


  @staticmethod
  def start(args):
    config = Config.from_yaml(args.config_dir)
    repo = Repository.from_yaml(os.path.join(config.repos_dir, f"{args.name}.yaml"), config.config_dir)
    #start_app(app)
      

  @staticmethod
  def stop(args):
    config = Config.from_yaml(args.config_dir)
    repo = Repository.from_yaml(os.path.join(config.repos_dir, f"{args.name}.yaml"), config.config_dir)
    #stop_app(app)


  @staticmethod
  def save_repo(repo):
    try:
      config_file = os.path.join(repo.config.repos_dir, f"{repo.name}.yaml")
      config_fp = open(config_file, "w")
      YAML().dump(repo.to_dict(), config_fp)
      config_fp.close()
      Keycloak(repo.config.keycloak_user, 
               repo.config.keycloak_password, 
               repo.config.hostname).create_role(f"ownr_role_{repo.name}", 
                                            f"Authorization role for app {repo.name}", 
                                            "ownr")
    except Exception as e:
      raise e
    

  @staticmethod
  def remove_repo(config, repo):
    try:
      repo_yaml = os.path.join(config.repos_dir, f"{repo.name}.yaml")
      repo_dir = os.path.join(config.repos_dir, f"{repo.name}.git")
      if os.path.isdir(repo_dir):
        shutil.rmtree(repo_dir)
      if os.path.isfile(repo_yaml):
        os.unlink(repo_yaml)      
      Keycloak(config.keycloak_user, 
               config.keycloak_password, 
               config.hostname).remove_role(f"ownr_role_{repo.name}", 
                                             "ownr")               
      #stop_app(app)
    except Exception as e:
      raise e


  @staticmethod
  def list_repositories(config):
    return [Repository.from_yaml(os.path.join(config.repos_dir, f), config.config_dir).to_dict()
            for f in os.listdir(config.repos_dir) 
            if os.path.isfile(os.path.join(config.repos_dir, f))]


  @staticmethod
  def list(args, format=True):
    config = Config.from_yaml(args.config_dir)
    if not format:
      return Repository.list_repositories(config)

    for name in Repository.list_repositories(config):
      print(name)