import logging
import argparse
import os
import uuid
from ruamel.yaml import YAML
import subprocess
from yaspin import yaspin
import docker
from functools import lru_cache
import kubernetes
from .keycloak import Keycloak

logger = logging.getLogger(__name__)

default_config = {
  "config_dir": "~/.ownr/",
  "license": os.getenv("license", ""),
  "platform_name": os.getenv("platform_name", "ownR platform"),
  "hostname": os.getenv("hostname", ""),
  "stack_name": os.getenv("stack_name", "ownR"),
  "port": int(os.getenv("port", 80)),
  "disable_ide": "True" == os.getenv("disable_ide", "False"),
  "major_version": os.getenv("major_version", "2019-Q3"),
  "executor_version": os.getenv("executor_version","2019-Q3"),
  "python_versions": eval(os.getenv("python_versions", "['2.7', '3.5', '3.6', '3.7']")),
  "R_versions": eval(os.getenv("R_versions", "['3.2', '3.3', '3.4', '3.5', '3.6']")),
  "julia_versions": eval(os.getenv("julia_versions", "['1.5']")),
  "ide_version": os.getenv("ide_version", "2019-Q3"),
  "notebook_version": os.getenv("notebook_version", "2019-Q3"),
  "influxdb": os.getenv("influxdb", "1.7.7"),
  "cadvisor": os.getenv("cadvisor", "v0.28.5"),
  "couchdb": os.getenv("couchdb", "2.3.1"),
  "redis": os.getenv("redis", "4.0"),
  "rabbitmq": os.getenv("rabbitmq", "3"),
  "keycloak": os.getenv("keycloak", "5.0.0"),
  "keycloak_user": os.getenv("keycloak_user", "admin"),
  "keycloak_password": os.getenv("keycloak_password", None),
  "postgres": os.getenv("postgres", "10"),
  "postgres_user": os.getenv("postgres_user", "keycloak"),
  "postgres_password": os.getenv("postgres_password", None),
  "k8s_token": os.getenv("k8s_token", None),
  "k8s_host": os.getenv("k8s_host", None),
  "mount_point": os.getenv("mount_point", False),
  }


def exec_shell(args):
  try:
    gen_command = subprocess.Popen(args, stdin=subprocess.PIPE)
    gen_command.communicate()
    gen_command.terminate()
  except Exception as e:
    raise e


class Config():

  @staticmethod
  def init_dir(path):        
    if os.path.isfile(path):
      logger.error(f"{path} is a file!")
      raise FileExistsError(f"{path} is a file!")

    if not os.path.isdir(path):
      os.mkdir(path)
  
  def __init__(self, args, default_config = default_config):
    if isinstance(args, argparse.Namespace):
      args = vars(args)

    if not args.get("license", default_config.get("license", None)):
      raise AttributeError("license is missing from arguments!")
    
    if not args.get("config_dir", default_config.get("config_dir", None)):
      raise AttributeError("config-dir is missing from arguments!")

    self.args = args
    
    self.config_dir = str(os.path.expanduser(args.get("config_dir", False) or default_config.get("config_dir")))
    self.apps_dir = os.path.join(self.config_dir, "apps")
    self.repos_dir = os.path.join(self.config_dir, "repos")
    self.init_dir(self.config_dir)
    self.init_dir(self.apps_dir)
    self.init_dir(self.repos_dir)

    self.license = args.get("license", False) or default_config["license"]
    self.hostname = args.get("hostname", False) or default_config["hostname"]
    self.platform_name = args.get("platform_name", False) or default_config["platform_name"]
    self.stack_name = args.get("stack_name", False) or default_config["stack_name"]
    self.port = args.get("port", False) or default_config["port"]
    self.disable_ide = args.get("disable_ide", False) or default_config["disable_ide"]
    self.major_version = args.get("major_version", False) or default_config["major_version"]
    self.executor_version = args.get("executor_version", False) or default_config["executor_version"]
    self.python_versions = args.get("python_versions", False) or default_config["python_versions"]
    self.R_versions = args.get("R_versions", False) or default_config["R_versions"]
    self.julia_versions = args.get("julia_versions", False) or default_config["julia_versions"]
    self.ide_version = args.get("ide_version", False) or default_config["ide_version"]
    self.notebook_version = args.get("notebook_version", False) or default_config["notebook_version"]
    self.influxdb = args.get("influxdb", False) or default_config["influxdb"]
    self.cadvisor = args.get("cadvisor", False) or default_config["cadvisor"]
    self.couchdb = args.get("couchdb", False) or default_config["couchdb"]
    self.redis = args.get("redis", False) or default_config["redis"]
    self.rabbitmq = args.get("rabbitmq", False) or default_config["rabbitmq"]
    self.keycloak = args.get("keycloak", False) or default_config["keycloak"]
    self.keycloak_user = args.get("keycloak_user", False) or default_config["keycloak_user"]
    self.keycloak_password = args.get("keycloak_password", False) or default_config.get("keycloak_password", str(uuid.uuid4()))
    self.postgres = args.get("postgres", False) or default_config["postgres"]
    self.postgres_user = args.get("postgres_user", False) or default_config["postgres_user"]
    self.postgres_password = args.get("postgres_password", False) or default_config.get("postgres_password", str(uuid.uuid4()))    
    self.k8s_host = args.get("k8s_host", False) or default_config.get("k8s_host", "")
    self.k8s_token = args.get("k8s_token", False) or default_config.get("k8s_token", "")
    self.secure_docker = args.get("secure_docker", False) or default_config.get("secure_docker", False)
    self.mount_point = args.get("mount_point", False) or default_config.get("mount_point", False)
    

  @property
  @lru_cache()  
  def engine(self):
    if self.secure_docker:
      os.environ["DOCKER_HOST"] = f"{self.hostname}:2376"
      os.environ["DOCKER_CERT_PATH"] = self.config_dir

    try:
      engine = docker.from_env()
      logger.info(f"Connected to ownR instance {self.hostname}")
    except Exception as e:
      logger.error(f"Error connecting to ownR instance {self.hostname}: {str(e)}")
      raise(e)
    return engine


  @staticmethod
  def get_config_file(config_dir, check_exists=True):
    config_dir = str(os.path.expanduser(config_dir))
    if not os.path.isdir(config_dir):
      if check_exists:
        logger.error(f"{config_dir} does not exists!")
        raise FileNotFoundError(f"{config_dir} does not exists!")
      try:
        os.mkdir(config_dir)
      except:
        logger.error(f"Cannot create {config_dir}!")
        raise PermissionError(f"Cannot create {config_dir}!")
    
    return str(os.path.join(config_dir, "ownr.yaml"))


  @staticmethod
  def set(args):    
    try:
      old_config = Config.from_yaml(args.config_dir, check_exists=False)
    except Exception as e:
      logger.error("Config file doesn't exists!")
      raise e

    setting = args.setting.replace("-", "_")

    if setting == "R_versions" or setting == "python_versions" or setting == "julia_versions":
      setattr(old_config, setting, args.value.split(","))
    else:
      setattr(old_config, setting, args.value)
    old_config.to_yaml(args.config_dir)


  @staticmethod
  def create(args):
    if args.gen_keys:
      Config.gen_keys(args)


    if args.interactive:
      try:
        old_config = Config.from_yaml(args.config_dir, check_exists=False)
      except:
        old_config = Config({}, default_config)

      old_config.license = eval('input(f"ownR license [{old_config.license}]: ")') \
        or old_config.license
      old_config.hostname = eval('input(f"hostname [{old_config.hostname}]: ")') \
        or old_config.hostname
      old_config.platform_name = eval('input(f"display name on the UI [{old_config.platform_name}]: ")') \
        or old_config.platform_name
      old_config.stack_name = eval('input(f"display name on the docker CLI [{old_config.stack_name}]: ")') \
        or old_config.stack_name
      old_config.port = eval('input(f"port to listen [{old_config.port}]: ")') \
        or old_config.port
      old_config.disable_ide = eval('input(f"disable IDE [{old_config.disable_ide}]: ")') \
        or old_config.disable_ide
      old_config.major_version = eval('input(f"ownR major version [{old_config.major_version}]: ")') \
        or old_config.major_version
      old_config.executor_version = eval('input(f"ownR executor version [{old_config.executor_version}]: ")') \
        or old_config.executor_version
      old_config.ide_version = eval('input(f"ownR IDE version [{old_config.ide_version}]: ")') \
        or old_config.ide_version
      old_config.notebook_version = eval('input(f"ownR notebbok version [{old_config.notebook_version}]: ")') \
        or old_config.notebook_version
      old_config.python_versions = (eval("""input(f"supported python versions [{','.join(old_config.python_versions)}]: ")""") \
        or ",".join(old_config.python_versions)).split(",")      
      old_config.R_versions = (eval("""input(f"supported R versions [{','.join(old_config.R_versions)}]: ")""") \
        or ",".join(old_config.R_versions)).split(",")
      old_config.julia_versions = (eval("""input(f"supported julia versions [{','.join(old_config.julia_versions)}]: ")""") \
        or ",".join(old_config.julia_versions)).split(",")      
      old_config.influxdb = eval('input(f"influxdb [{old_config.influxdb}]: ")') \
        or old_config.influxdb
      old_config.cadvisor = eval('input(f"cadvisor [{old_config.cadvisor}]: ")') \
        or old_config.cadvisor
      old_config.couchdb = eval('input(f"couchdb [{old_config.couchdb}]: ")') \
        or old_config.couchdb
      old_config.redis = eval('input(f"redis [{old_config.redis}]: ")') \
        or old_config.redis
      old_config.rabbitmq = eval('input(f"rabbitmq [{old_config.rabbitmq}]: ")') \
        or old_config.rabbitmq
      old_config.keycloak = eval('input(f"keycloak [{old_config.keycloak}]: ")') \
        or old_config.keycloak
      old_config.keycloak_user = eval('input(f"keycloak admin user [{old_config.keycloak_user}]: ")') \
        or old_config.keycloak
      old_config.keycloak_password = eval('input(f"keycloak admin password [{old_config.keycloak_password}]: ")') \
        or old_config.keycloak_password
      old_config.postgres = eval('input(f"postgres [{old_config.postgres}]: ")') \
        or old_config.postgres

      old_config.to_yaml(args.config_dir)      
      return
    
    try:
      old_config = Config.from_yaml(args.config_dir, check_exists=False).to_dict()
    except:
      old_config = default_config

    Config(args, old_config).to_yaml(args.config_dir)
    

  def to_dict(self):
    return {"version": 1, 
            "license": self.license,
            "platform_name": self.platform_name,
            "hostname": self.hostname,
            "stack_name": self.stack_name,
            "port": self.port,
            "disable_ide": self.disable_ide,
            "major_version": self.major_version,
            "executor_version": self.executor_version,
            "python_versions": self.python_versions,
            "R_versions": self.R_versions,
            "julia_versions": self.julia_versions,
            "notebook_version": self.notebook_version,
            "ide_version": self.ide_version,
            "influxdb": self.influxdb,
            "cadvisor": self.cadvisor,
            "couchdb": self.couchdb,
            "redis": self.redis,
            "rabbitmq": self.rabbitmq,
            "keycloak": self.keycloak,
            "keycloak_user": self.keycloak_user,
            "keycloak_password": self.keycloak_password,
            "postgres": self.postgres,
            "postgres_user": self.postgres_user,
            "postgres_password": self.postgres_password,
            "k8s_host": self.k8s_host,
            "k8s_token": self.k8s_token,
            "secure_docker": self.secure_docker,
            "mount_point": self.mount_point
    }

  def to_yaml(self, config_dir):
    try:
      config = Config.get_config_file(config_dir)
      config_fp = open(config, "w")
      YAML().dump(self.to_dict(), config_fp)
      config_fp.close()
    except Exception as e:
      raise e

  @staticmethod
  def from_yaml(config_dir="~/.ownr", check_exists=True):
    config = Config.get_config_file(config_dir, check_exists=check_exists)
    if not os.path.isfile(config) and check_exists:
      logger.warn(f"ownR config file {config} does not exists! Loading from environment'") 
      try:
        return Config({})
      except Exception:
        exit(1)

    try:
      config_fp = open(config, "r")
      data = YAML().load(config_fp)    
      data["config_dir"] = config_dir
      config_fp.close()
    except Exception as e:
      raise e

    return Config(data)

  @staticmethod
  def get_parser(management_commands):
    parser = management_commands.add_parser("config", help="Manage the ownR config")
    parser._positionals.title = argparse.SUPPRESS
    commands = parser.add_subparsers(help="", metavar="Command", dest="subcommand")
        
    create_command = commands.add_parser("create", help="Create the ownR config file")
    create_command.add_argument("-l", "--license", required=True, help="ownR license")
    create_command.add_argument("-i", "--interactive", action="store_true", help="interactive setup")        
    create_command.add_argument("--gen-keys", action="store_true", help="ownR license")      
    create_command.add_argument("--platform-name", help="display name on the UI")
    create_command.add_argument("--hostname", help="hostname")
    create_command.add_argument("--stack-name", help="display name in the docker CLI")
    create_command.add_argument("--port", help="port to listen")
    create_command.add_argument("--disable-ide", action="store_true", help="turn off IDE in the platform")
    create_command.add_argument("--major-version", help="ownR major version")
    create_command.add_argument("--executor-version", help="ownR executor version")
    create_command.add_argument("--notebook-version", help="ownR notebook version")
    create_command.add_argument("--ide-version", help="ownR ide version")     
    create_command.add_argument("--python-versions", help="list of supported python versions")
    create_command.add_argument("--R-versions", help="list of supported R versions")
    create_command.add_argument("--julia-versions", help="list of supported julia versions")
    create_command.add_argument("--influxdb", help="influxdb version")
    create_command.add_argument("--cadvisor", help="cadvisor version")
    create_command.add_argument("--couchdb", help="couchdb version")
    create_command.add_argument("--redis", help="redis version")
    create_command.add_argument("--rabbitmq", help="rabbitmq version")
    create_command.add_argument("--keycloak", help="keycloak version")
    create_command.add_argument("--keycloak-user", help="keycloak admin user")
    create_command.add_argument("--keycloak-password", help="keycloak password")
    create_command.add_argument("--postgres", help="postgres version")
    create_command.add_argument("--postgres-user", help="postgres user")
    create_command.add_argument("--postgres-password", help="postgres password")


    set_command = commands.add_parser("set", help="Change the value of a specific setting")
    set_command._positionals.title = argparse.SUPPRESS
    set_command.add_argument("setting", choices=["license",
                                                 "platform-name",
                                                 "hostname",
                                                 "stack-name",
                                                 "port",
                                                 "disable-ide",
                                                 "major-version",
                                                 "executor-version",
                                                 "notebook-version",
                                                 "ide-version",
                                                 "python-versions",
                                                 "R-versions",
                                                 "julia-versions",
                                                 "influxdb",
                                                 "cadvisor",
                                                 "couchdb",
                                                 "redis",
                                                 "rabbitmq",
                                                 "keycloak",
                                                 "keycloak_user",
                                                 "keycloak_password",
                                                 "postgres",
                                                 "postgres_user",
                                                 "postgres_password"],
                             help="name of the setting to change",
                             metavar="setting")
    set_command.add_argument("value", help="value of the setting")                         

    k8s_command = commands.add_parser("k8s", help="deploy to kubernetes")
    return parser


  @staticmethod
  def k8s(args):
    if isinstance(args, argparse.Namespace):
      args = vars(args)

    config = Config.from_yaml(args.get("config_dir")).to_dict()
    for keys in config: 
        config[keys] = str(config[keys])

    configuration = kubernetes.client.Configuration()
    configuration.api_key["authorization"] = config.get("k8s_token") #"eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJkZWZhdWx0Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZWNyZXQubmFtZSI6Im93bnItYWRtaW4tdG9rZW4tejZxbW0iLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoib3duci1hZG1pbiIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjU3MzU4ZmZjLWVjMjUtMTFlOS1iYTRjLTQyMDEwYWE0MDAwMiIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDpkZWZhdWx0Om93bnItYWRtaW4ifQ.HsVIahMM5NLtfhRgEeBgy5A_KQqv3bR7HD17CUdd4Edgo6hQadBEsupgkFSV0h1ppuTsuCio4ZKJeU2J9jJFuqrTt7cE6UxMFZpZUhe4fsPk-YmTXXjKRo-STftx3yahTjD7ikUusbKzyFm5-GR1l67AY0h6QHjTY6OusxITcfW1FKBZ5Sigg-_S_WEQZoHXsKtX4MCUUBotlSfK3PnbxiVHrYqBY5bj0Z3mJ313p1M6rWd6-U7_5KiXKWxjhmBMe6xRDdbevDjgCbEQYKO6TKS6LqY6z2QekbTaUyANM14WYDvprOPlCgjTU9fI9lz0UkpwywDui77lP73yrx5KGw"
    configuration.api_key_prefix['authorization'] = 'Bearer'
    configuration.verify_ssl = False
    configuration.host = config.get("k8s_host") #'https://34.90.91.253' # kubectl cluster-info
    core_api = kubernetes.client.CoreV1Api(kubernetes.client.ApiClient(configuration))
    name = "ownr-config"
    try:
      core_api.patch_namespaced_config_map(name, "default", {"data": config}, force=True)
      logger.warn("ownR config changed.")
    except kubernetes.client.rest.ApiException as e:
      #if e.reason == "Not Found":
      core_api.create_namespaced_config_map("default", {
        "apiVersion": "v1",
        "kind": "ConfigMap",
        "metadata": {
          "name": "ownr-config"
        },
        "data": config
      })
      logger.warn("ownR config created.")
      # else:
      #  raise e
    

  @staticmethod
  def gen_keys(args):
    if isinstance(args, argparse.Namespace):
      args = vars(args)
      
    config_dir = str(os.path.expanduser(str(args.get("config_dir"))))
    Config.get_config_file(config_dir, check_exists=False)

    config_dir = str(os.path.expanduser(str(args.get("config_dir"))))
    Config.get_config_file(config_dir, check_exists=False)
    passphrase = str(uuid.uuid4())
    hostname = args.get("hostname")

    gen_script = f"""
cd {config_dir}    
openssl genrsa -passout pass:{passphrase} -aes256 -out ca-key.pem 4096
openssl req -new -x509 -passin pass:{passphrase} -days 365 -key ca-key.pem -sha256 -out ca.pem \
  -subj "/CN={hostname}"
openssl genrsa -out server-key.pem 4096
openssl req -subj "/CN={hostname}" -sha256 -new -key server-key.pem -out server.csr
echo subjectAltName = DNS:{hostname},IP:127.0.0.1 >> extfile.cnf
echo extendedKeyUsage = serverAuth >> extfile.cnf
openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out server-cert.pem -extfile extfile.cnf -passin pass:{passphrase}
openssl genrsa -out key.pem 4096
openssl req -subj '/CN=client' -new -key key.pem -out client.csr
echo extendedKeyUsage = clientAuth > extfile-client.cnf
openssl x509 -req -days 365 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out cert.pem -extfile extfile-client.cnf -passin pass:{passphrase}
rm -v client.csr server.csr extfile.cnf extfile-client.cnf
chmod -v 0400 ca-key.pem key.pem server-key.pem
chmod -v 0444 ca.pem server-cert.pem cert.pem
"""
    with yaspin(text="Generating SSL keys ..."):
      gen_command = subprocess.Popen(["bash"], stdin=subprocess.PIPE, 
                                    stdout=subprocess.DEVNULL,
                                    stderr=subprocess.DEVNULL)
      gen_command.communicate(gen_script.encode())
      gen_command.terminate()

    logger.info("Passphrase used for generating SSL keys:")
    logger.info(f"\t{passphrase}")
              
  
