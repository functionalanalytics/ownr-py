FROM eik7p3lr755i5fzwxmo6pvru50la3vhs.ownr.io/ownr-base:dev
RUN apt-get update
RUN apt-get install -y python3-setuptools
RUN mkdir -p /opt/ownr
WORKDIR /opt/ownr
COPY ./ ./
RUN python3 setup.py sdist

